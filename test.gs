function myFunction() {
  accessToken = getAAMtoken();
var ss = SpreadsheetApp.getActive();
var ssInput = ss.getSheetByName("Countries & Brands");
var rows = ssInput.getLastRow();
  
  for (r = 46; r<=rows; r++) {
 //get folder name to name traits
    var folderId = ssInput.getRange(r, 6).getValue()
    var options = {'method':'get', 'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'}  } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits/"+folderId, options);
    var folderName = JSON.parse(response).name;
    
    ssInput.getRange(r, 9).setValue(folderName)
  
  }
    
}


function deleteTraits() {

  accessToken = getAAMtoken();
var ss = SpreadsheetApp.getActive();
var ssdelete = ss.getSheetByName("deletetraits");
var rows = ssdelete.getLastRow();
  
var IDs = ssdelete.getRange(1, 1, rows, 1).getValues();
  
  for (r = 0; r<rows; r++) {
       
    var options = {'method':'delete',
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits/"+IDs[r],
                                   options) 
    
    ssdelete.getRange(r+1, 2).setValue(response.getResponseCode())
    
       
       }

}


function updateTraits () {

  accessToken = getAAMtoken();
ss = SpreadsheetApp.getActive();
ssCopy = ss.getSheetByName("Copy of OUTPUT - products");
rows = ssCopy.getLastRow();  
  
  for (r = 3; r<=rows; r++) {
  
  var sid = ssCopy.getRange(r, 14).getValue();
  
        var traitDefinitionView = {
        "name": ssCopy.getRange(r, 9).getValue(),
        "description": ssCopy.getRange(r, 12).getValue(),
        //"integrationCode": traitIntegrationCode,
        "comments": ssCopy.getRange(r, 13).getValue(),
        "traitType": "RULE_BASED_TRAIT",
        "status": "ACTIVE",
        "dataSourceId": ssCopy.getRange(r, 6).getValue(),
        "folderId": ssCopy.getRange(r, 7).getValue(),
        "traitRule": ssCopy.getRange(r, 10).getValue(),
        "categoryId": 0,
        "ttl": "720"
      }
 
         var options = {'method':'put',
                        'payload': JSON.stringify(traitDefinitionView),
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/traits/" + sid,
                                 options)  
    
    ssCopy.getRange(r, 16).setValue(response);

    }

}

function nl() {

 accessToken = getAAMtoken();
var ss = SpreadsheetApp.getActive();
var nl = ss.getSheetByName("nl");
var rows = nl.getLastRow();

   var options = {'method':'get',
                       // 'payload': JSON.stringify(traitDefinitionView),
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/traits/?folderId=632597",
                                 options)  
  
    data = JSON.parse(response)
    
    for (n=1; n< data.length; n++) {
    nl.getRange(n, 1).setValue(data[n].name)
    nl.getRange(n, 2).setValue(data[n].sid)
    }
}

function nl2() {

  

  
  accessToken = getAAMtoken();
var ss = SpreadsheetApp.getActive();
var nl = ss.getSheetByName("nl");
var rows = nl.getLastRow();

  for (r=1; r<=rows; r++) {
  
    var sid = nl.getRange(r, 2).getValue();
    var name = nl.getRange(r, 1).getValue();
       
      var body = {
  "folderId": 800170,
  "traitType": "RULE_BASED_TRAIT",
  "name":name,
  "dataSourceId":183034,
  "status": "ACTIVE"
}
    
   var options = {'method':'put',
                        'payload': JSON.stringify(body),
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/traits/"+sid,
                                 options)  

    nl.getRange(r, 4).setValue(response);
    }

}