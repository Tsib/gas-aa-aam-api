function AAMnewReportSuite() {
  
  accessToken = getAAMtoken()
  var ss = SpreadsheetApp.getActive();
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  var rowsI = ssInput.getLastRow();
  ssFolders = ss.getSheetByName("OUTPUT - folder setup");
  var ui = SpreadsheetApp.getUi();
  
     ssFolders.getRange(2, 1, 500, 40).clear();
  
  var ssRef = ss.getSheetByName("Countries & Brands");
  var marketFolders = {"AR":"726249", "AT":"729330", "BR":"682571", "CA":"723975", "CH":"728219", "CO":"720578", "DE":"699324", "ES":"714449", "RU":"737640",
                       "SA":"737639", "PT":"737642", "FR":"714448", "GB":"632585", "ID":"680363", "IE":"733768", "IN":"656438", "IT":"714465", "MX":"709534", 
                       "NL":"632584", "PH":"680366", "SE":"733776", "TH":"678308", "TR":"714468", "US":"647911", "VN":"677967", "ZA":"713295", "DK":"737641",
                       "BO":"737647", "CL":"737644", "EC":"737646", "PY":"737645", "UY":"737643", "PE":"742822", "PR":"742824"
                      };
  var marketSegmentFolders = {"AR":"221033", "BO":"245537", "BR":"202644", "CL":"245538", "CO":"221031", "EC":"245539", "MX":"221032", "PE":"245540", 
                              "PR":"245541", "PY":"245542", "UY":"245543", "AT":"221020", "BA":"245505", "BE":"245506", "BG":"245507", "CH":"221019",
                              "DE":"217662", "DK":"245508", "ES":"221015", "FI":"245509", "FR":"221014", "GB":"169028", "GR":"245510", "HR":"245511",
                              "HU":"245512", "IE":"226383", "IT":"221016", "NL":"169030", "PL":"245513", "PT":"245514", "RO":"245515", "RS":"245516",
                              "RU":"245517", "SA":"245518", "SE":"226389", "SI":"245519", "SK":"245520", "TR":"221017", "UA":"245521", "ZA":"221018",
                              "CA":"222454", "US":"222455", "AU":"245522", "BD":"245523", "CN":"245524", "EG":"245525", "HK":"245526", "ID":"222141",
                              "IL":"245527", "IN":"222142", "IR":"245528", "JP":"245529", "LB":"245530", "LK":"245531", "MY":"245532", "NZ":"245533",
                              "PH":"222143", "PK":"245534", "SG":"245535", "TH":"213298", "TW":"245536", "VN":"222144"
                             };
  
  
  //loop through Report Suites to configure
  for (r = 6; r<= rowsI; r++)  {
       
       //check that report suite is marked for set up
    if ( ssInput.getRange(r, 9).getValue()  !== "NO") {

      var option = ssInput.getRange(r, 9).getValue();
      var suite = ssInput.getRange(r, 1).getValue();
      var split = suite.split("-");
  var market = split[split.length-1].toUpperCase();
  var brand = split[split.length-2];
  brand = brand.charAt(0).toUpperCase() + brand.substr(1)    
  var marketFolder = marketFolders[market];
  var marketSegmentFolder = marketSegmentFolders[market];
  
   
      if (option == "Create Data Source") {   
    var dataSourceId = AAMnewDataSource(market); 
      } else if (option == "Create Trait Folders") {
        if (marketFolder == undefined ) { ui.alert("Market Trait folder ID not present in code reference, add in Script Editor, AAMconfigureReportSuite.gs"); return; }
    var folders = AAMbrandFolder(market, brand, marketFolder, suite);
      } else if (option == "Create Trait Folders & Data Source") {
        if (marketFolder == undefined ) { ui.alert("Market Trait folder ID not present in code reference, add in Script Editor, AAMconfigureReportSuite.gs"); return; }
      var dataSourceId = AAMnewDataSource(market); 
      var folders = AAMbrandFolder(market, brand, marketFolder, suite);
      } else if (option == "Create Segment Folders") {
        if (marketSegmentFolder == undefined ) { ui.alert("Market Segment folder ID not present in code reference, add in Script Editor, AAMconfigureReportSuite.gs"); return; }
      var folders = AAMbrandSegmentFolder(market, brand, marketSegmentFolder);
      }
      
       
      
      ssInput.getRange(r, 2).setValue("Set up complete");
      
    }
   }
  
  
  
}


//function to create new Onsite Data Source
function AAMnewDataSource(market) {

  accessToken = getAAMtoken()
  var ss = SpreadsheetApp.getActive();
  var ssFolders = ss.getSheetByName("OUTPUT - folder setup");
  var ssDatasources = ss.getSheetByName("Data Source IDs");
  
  var dsDefinition = {
  "idType": "COOKIE",
  "outboundS2S": false,
  "name": market + " - Onsite Data",
  "description": "Data source used for all "+market+" onsite data capture",
  "inboundS2S": true,
  "useAudienceManagerVisitorID": true,
  "marketingCloudVisitorIdVersion": 2,
  "type": "GENERAL"
}
  
    var options = {'method':'post', 'payload': JSON.stringify(dsDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/datasources",
                                   options)     
    var data = JSON.parse(response);
  
  ssFolders.getRange(2, 1).setValue(market);
  ssFolders.getRange(2, 4).setValue(data.dataSourceId);
  
  rowsDSref = ssDatasources.getLastRow()+1;
  //write new Data Source details to DS reference sheet
  ssDatasources.getRange(rowsDSref, 1).setValue(market + " - Onsite Data");
  ssDatasources.getRange(rowsDSref, 2).setValue(market);
  ssDatasources.getRange(rowsDSref, 3).setValue(data.dataSourceId);
  
  return data.dataSourceId;

}


//Function to create the new TRAIT Brand folder and the standard trait folders under it
function AAMbrandFolder(market, brand, marketFolder, suite) {

  accessToken = getAAMtoken()
  var ss = SpreadsheetApp.getActive();
  var ssFolders = ss.getSheetByName("OUTPUT - folder setup");
  var rowsF = ssFolders.getLastRow();
  var ssRef = ss.getSheetByName("Countries & Brands");
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");

  var folders = {};
  
  var folderDefinition = {
  "parentFolderId": marketFolder,
  "name": market +"_"+ brand
}
  
  //create Brand folder
  var options = {'method':'post', 'payload': JSON.stringify(folderDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits",
                                   options)     
    var data = JSON.parse(response);
    var brandFolderId = data.folderId;
    folders.brand = brandFolderId;
  
  
  ssFolders.getRange(rowsF+1, 1).setValue(market);
  ssFolders.getRange(rowsF+1, 2).setValue(market+"_"+brand);
  ssFolders.getRange(rowsF+1, 5).setValue(data);
  
  //create standard brand Subfolders
  var standardFolders = ["App","Campaign","Display","Ecom","Email","Onsite","Partner","Search","Social"]
  
  for (var s=0; s<standardFolders.length; s++) {
      
    folderDefinition = {
  "parentFolderId": brandFolderId,
  "name": standardFolders[s]
}
   
  var options = {'method':'post', 'payload': JSON.stringify(folderDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits",
                                   options)     
    var data = JSON.parse(response);
    
    ssFolders.getRange(rowsF+1+s, 1).setValue(market);
    ssFolders.getRange(rowsF+1+s, 2).setValue(market+"_"+brand);
    ssFolders.getRange(rowsF+1+s, 3).setValue(standardFolders[s]);
    ssFolders.getRange(rowsF+1+s, 4).setValue(data.folderId);
    ssFolders.getRange(rowsF+1+s, 5).setValue(data);
    
    if ( s == 5 ) {folders.onsite = data.folderId;}
    
    var rowsR = ssRef.getLastRow();
      //write new folder references to reference sheet for later trait creation
      ssRef.getRange(rowsR+1, 1).setValue(ssInput.getRange(1, 2).getValue());
      ssRef.getRange(rowsR+1, 2).setValue(suite);
      ssRef.getRange(rowsR+1, 3).setValue(market);
      ssRef.getRange(rowsR+1, 4).setValue(marketFolder);
      ssRef.getRange(rowsR+1, 5).setValue(market+"_"+brand);
      ssRef.getRange(rowsR+1, 6).setValue(folders.brand);
      ssRef.getRange(rowsR+1, 7).setValue(folders.onsite);
    
       }
  //return object with brand and onsite folder IDs to be used by the master function that writes to the ref sheet
  return folders;

}

//Function to create the new SEGMENT Brand folder and the standard segment folders under it
function AAMbrandSegmentFolder(market, brand, marketSegmentFolder) {

  accessToken = getAAMtoken()
  var ss = SpreadsheetApp.getActive();
  var ssFolders = ss.getSheetByName("OUTPUT - folder setup");
  var rowsF = ssFolders.getLastRow();
  var ssSegmentFoldersRef = ss.getSheetByName("AAM segment folders");
  var rowsRefS = ssSegmentFoldersRef.getLastRow();
  
  var folders = {};
  
  var folderDefinition = {
  "parentFolderId": marketSegmentFolder,
  "name": market +"_"+ brand
}
  
  //create Brand folder
  var options = {'method':'post', 'payload': JSON.stringify(folderDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/folders/segments",
                                   options)     
    var data = JSON.parse(response);
    var brandFolderId = data.folderId;
    folders.brand = brandFolderId;
  
  
  ssFolders.getRange(rowsF+1, 1).setValue(market);
  ssFolders.getRange(rowsF+1, 2).setValue(market+"_"+brand);
  ssFolders.getRange(rowsF+1, 5).setValue(data);
  
  //create standard brand Subfolders
  var standardFolders = ["Analysis","Digital Media","Persona","Targeting"]
  
  for (var s=0; s<standardFolders.length; s++) {
      
    folderDefinition = {
  "parentFolderId": brandFolderId,
  "name": standardFolders[s]
}
   
  var options = {'method':'post', 'payload': JSON.stringify(folderDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/folders/segments",
                                   options)     
    var data = JSON.parse(response);
    rowsRefS++
    ssFolders.getRange(rowsF+1+s, 1).setValue(market);
    ssFolders.getRange(rowsF+1+s, 2).setValue(market+"_"+brand);
    ssFolders.getRange(rowsF+1+s, 3).setValue(standardFolders[s]);
    ssFolders.getRange(rowsF+1+s, 4).setValue(data.folderId);
    ssFolders.getRange(rowsF+1+s, 5).setValue(data);
    
    //write segment folder details to "AAM segment folders" reference sheet
    ssSegmentFoldersRef.getRange(rowsRefS, 1).setValue(market);
    ssSegmentFoldersRef.getRange(rowsRefS, 2).setValue(marketSegmentFolder);
    ssSegmentFoldersRef.getRange(rowsRefS, 3).setValue(market +"_"+ brand);
    ssSegmentFoldersRef.getRange(rowsRefS, 4).setValue(brandFolderId);
    ssSegmentFoldersRef.getRange(rowsRefS, 5).setValue(data.folderId);
    ssSegmentFoldersRef.getRange(rowsRefS, 6).setValue(standardFolders[s]);
    
       }
  //return object with brand and onsite folder IDs to be used by the master function that writes to the ref sheet
  return folders;

}
