function AAauth() {

  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  var region = ssInput.getRange(1, 2).getValue();
 
switch(region) {
    case "EMEA":
        var username = "panos.tsimpos@unilever.com:Unilever";
        var password = "56feea21705a481f65d489ec8ac8dc7a";
        break;
    case "APAC":
        var username = "panos.tsimpos@unilever.com:Unilever APAC";
        var password = "9d8e15b0caa1fdf8f706d929f275ea3f";
        break;
    case "NA":
        var username = "panos.tsimpos@unilever.com:Unilever NA";
        var password = "042f744da46f4c7063693f1f10ca1702";
        break;
    case "LATAM":
        var username = "panos.tsimpos@unilever.com:Unilever LATAM";
        var password = "f3dd87bbb7301f4ed4b85836814a4c4f";
        break;
}
  
  
  
    //generate Authentication headers from credentials with library
wsse =   new Wsse();
return headers = wsse.generateAuth(username, password);

}

function AAsuites() {

var ss = SpreadsheetApp.getActive();
var ssSuites = ss.getSheetByName("AA suites");
ssSuites.clear({contentsOnly: true });
  
  var options = {'method':'post',
                   'payload': JSON.stringify( {
	"search":"",
	"types":[
		"standard"
	]
} ),
                   'headers': AAauth(),
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.omniture.com/admin/1.4/rest/?method=Company.GetReportSuites",
                                   options) 

    var reportSuites = JSON.parse(response).report_suites;
  
  for (s=0; s<reportSuites.length; s++) {
       
    ssSuites.getRange(s+1, 2).setValue(reportSuites[s].rsid);   
    ssSuites.getRange(s+1, 1).setValue(reportSuites[s].site_title);
    
       }
  
}

function AAqueue() {

var ss = SpreadsheetApp.getActive();
var ssInput = ss.getSheetByName("INPUT - Options");
ssInput.getRange(6, 2, 100,1).clear({contentsOnly: true });
var ssOutputProd = ss.getSheetByName("OUTPUT - products");
var rows = ssInput.getLastRow();
var reportIDs = [];
var folderNames = [];
var reportSuiteList = [];
var dataSourceIDs = [];
var folderIDs = [];

  for (r = 6; r <= rows; r++) {
  
    
//define report
var reportSuiteID = ssInput.getRange(r,1).getValue();
var folderName = ssInput.getRange(r,7).getValue();
dataSourceIDs.push(ssInput.getRange(r,6).getValue());    
folderIDs.push(ssInput.getRange(r,5).getValue());    
var level = ssInput.getRange(r,8).getValue();
    
    //skip rows without a reportSuite ID
    if (reportSuiteID == "") {continue} 

var dateFrom = ssInput.getRange(1,5).getDisplayValue();
var dateTo = ssInput.getRange(2,5).getDisplayValue();

reportSuiteList.push(reportSuiteID);
  
    
var reportDescription = 
{
	"reportDescription":{
		"reportSuiteID":reportSuiteID,
		"dateFrom":dateFrom,
		"dateTo":dateTo,
		"metrics":[
			{
				"id":"event8"
			}
		],
		"elements":[
			{
				"id":"eVar28",
				"top":"100",
				"startingWith":"0"		}
		],
		"currentData":"false"}
}

//if operating on the product level then also request Product name and SKU
if (level == "PRODUCT") {
 reportDescription.reportDescription.elements.push( 
{				"id":"eVar29",
				"top":"100",
				"startingWith":"0"		},
{				"id":"eVar30",
				"top":"100",
				"startingWith":"0"		}
 )
} 

//add report to generation queue
 var options = {'method':'post',
                   'payload': JSON.stringify(reportDescription),
                   'headers': AAauth(),
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.omniture.com/admin/1.4/rest/?method=Report.Queue",
                                   options) 
    Logger.log(response)
    //write down report ID of queued report
    var reportID = JSON.parse(response).reportID;
    reportIDs.push(reportID);
    folderNames.push(folderName);
  
    ssInput.getRange(r, 2).setValue(reportID);
   
    //close report suite loop
       }   

  //pause script for 2 seconds to allow AA to run report before attempting to retrieve it
  Utilities.sleep(200);  
  AAget(reportIDs, reportSuiteList, dataSourceIDs, folderIDs, folderNames, level);
       
}

function AAget(reportIDs, reportSuiteList, dataSourceIDs, folderIDs, folderNames, level) {
  
  var ss = SpreadsheetApp.getActive();
  var ssInput = ss.getSheetByName("INPUT - Options");
  var ssOutputProd = ss.getSheetByName("OUTPUT - products");
  ssOutputProd.getRange(3,1, 1000,45).clear({contentsOnly: true });
  var Irows = ssInput.getLastRow();
  
  //define reportID if function is ran asynchronously from Menu instead of called from AAqueue()
  if ( typeof(reportIDs) == "undefined" ) {
  var reportIDs = ssInput.getRange(6, 2, Irows-5,1).getValues();
  var dataSourceIDs = ssInput.getRange(6, 6, Irows-5,1).getValues();
  var folderIDs = ssInput.getRange(6, 5, Irows-5,1).getValues();
  var reportSuiteList= ssInput.getRange(6, 1, Irows-5,1).getValues();
  var folderNames = ssInput.getRange(6,7,Irows-5,1 ).getValues();
  var level = ssInput.getRange(6,8,Irows-5,1).getValues();

  }
  
  
  //loop through report IDs to fetch
  for (t=0; t<reportIDs.length; t++) {
    Logger.log(reportIDs[t].toString())
    if (reportIDs[t] == "") {continue}
  
  var options = {'method':'post',
                   'payload': JSON.stringify( {"reportID": reportIDs[t].toString() } ),
                   'headers': AAauth(),
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.omniture.com/admin/1.4/rest/?method=Report.Get",
                                   options) 

  var report = JSON.parse(response);
  var reportData = report.report.data;
  var Orows = ssOutputProd.getLastRow();
    
  //loop through report results and write rows to sheet
  for (r= 0; r< reportData.length; r++) {
    
    
    //only try to write product name and SKU if operating on the product level
    if (level[r] == "PRODUCT") {
      for (s = 0; s < reportData[r].breakdown.length; s++) {
    ssOutputProd.getRange(r+Orows+1+s, 1).setValue(reportSuiteList[t]);
    ssOutputProd.getRange(r+Orows+1+s, 2).setValue(reportData[r].name);
    ssOutputProd.getRange(r+Orows+1+s, 3).setValue(reportData[r].breakdown[s].name);
    ssOutputProd.getRange(r+Orows+1+s, 4).setValue(reportData[r].breakdown[s].breakdown[0].name);
    ssOutputProd.getRange(r+Orows+1+s, 5).setValue(reportData[r].counts);
    ssOutputProd.getRange(r+Orows+1+s, 6).setValue(dataSourceIDs[t]);
    ssOutputProd.getRange(r+Orows+1+s, 7).setValue(folderIDs[t]);
    ssOutputProd.getRange(r+Orows+1+s, 8).setValue(folderNames[t]);    
           }
    } else { 
    ssOutputProd.getRange(r+Orows+1, 1).setValue(reportSuiteList[t]);
    ssOutputProd.getRange(r+Orows+1, 2).setValue(reportData[r].name);  
    ssOutputProd.getRange(r+Orows+1, 5).setValue(reportData[r].counts);
    ssOutputProd.getRange(r+Orows+1, 6).setValue(dataSourceIDs[t]);
    ssOutputProd.getRange(r+Orows+1, 7).setValue(folderIDs[t]);
    ssOutputProd.getRange(r+Orows+1, 8).setValue(folderNames[t]);
    }
    
       }

}
  
    
}