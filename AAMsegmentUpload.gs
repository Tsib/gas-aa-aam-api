function AllVisitsSegment(folderName, brandName, traitID, segmentAnalysisFolderId, dataSourceId, r) {
  
  //accessToken = getAAMtoken();
  
  ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - standard traits");
  
  //compile segment definition for campaign
  var segmentDefinition = {
  "name": folderName+"_Analysis_All Visitors",
  "description": "All visitors to the "+folderName+" site",
  "integrationCode": folderName+"_Analysis_All Visitors",
  "segmentRule": "("+traitID+"T)",
  "folderId": segmentAnalysisFolderId,
  "dataSourceId": dataSourceId
  //, "mergeRuleDataSourceId": 
}
  
  Logger.log(segmentDefinition);
  
    //make API request
    var options = {'method':'post',
                   'payload': JSON.stringify(segmentDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/segments",
                                   options) 
    ssOutput.getRange(r, 23).setValue(response.getResponseCode())
    ssOutput.getRange(r, 24).setValue(response.getContentText())
    Logger.log(response)
    
  if (response.getResponseCode() == 200 || response.getResponseCode() == 201) {
      ssOutput.getRange(r, 17).setValue( folderName+"_Analysis_All Visitors" );
      ssOutput.getRange(r, 18).setValue( "("+traitID+"T)" );
      ssOutput.getRange(r, 19).setValue( folderName+"_Analysis_All Visitors" );
      ssOutput.getRange(r, 20).setValue( "All visitors to the "+folderName+" site" );
      ssOutput.getRange(r, 21).setValue( JSON.parse(response).sid );
      
  }
   
  
}