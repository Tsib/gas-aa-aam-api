function getDestinations() {

  var ss = SpreadsheetApp.getActive();
  var ssDest = ss.getSheetByName("Segment Mapping");
  accessToken = getAAMtoken();
  ssDest.getRange(2,1,1000,10).clear({contentsOnly: true });
  ssDest.getRange(2,27,1000,2).clear({contentsOnly: true });

  
  //get a list of DESTINATIONS
    var options = {'method':'get',
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/destinations/", options) 
    Logger.log(response)
    var destinations = JSON.parse(response)
    var rFB = 2;
    
    //write out Facebook destinations
    for (d = 0; d < destinations.length; d++) {
         
      if (destinations[d].name.match("Facebook") !== null ) {
      ssDest.getRange(rFB, 2).setValue(destinations[d].name);
      ssDest.getRange(rFB, 3).setValue(destinations[d].destinationId);
      ssDest.getRange(rFB, 4).setValue(destinations[d].description);
      ssDest.getRange(rFB, 1).setValue(destinations[d].name.split(" -")[0]);
        rFB++;
        
          //get a list of MAPPED Segments for each destination to avoid duplicated mappings
    var options = {'method':'get',
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/destinations/"+destinations[d].destinationId+"/mappings/", options)      
        
    var mappings = JSON.parse(response);
        for (m = 0; m< mappings.length; m++) {
             
                ssDest.getRange(m+2, 27).setValue(mappings[m].elementName);
                ssDest.getRange(m+2, 28).setValue(mappings[m].traitAlias);

             }

        
      }
         }
  
  
  //get a list of SEGMENTS
    var options = {'method':'get',
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/segments/", options) 
    
    var segments = JSON.parse(response)
    
    for (s = 0; s < segments.length; s++) {
         
      ssDest.getRange(s+2, 6).setValue(segments[s].name);
      ssDest.getRange(s+2, 7).setValue(segments[s].sid);
      ssDest.getRange(s+2, 8).setValue(segments[s].description);
      ssDest.getRange(s+2, 9).setValue(segments[s].dataSourceId);
      ssDest.getRange(s+2, 10).setValue("aam"+segments[s].sid);
      ssDest.getRange(s+2, 11).setValue(segments[s].name.split("_")[0]);      
         }
  
  
 
  
   var options = {'method':'get',
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/segments/", options) 
  
  
}


function AAMmapSegment() {

  var ss = SpreadsheetApp.getActive();
  var ssDest = ss.getSheetByName("Segment Mapping");
  
  //get AAM API authorisation token
  accessToken = getAAMtoken();
rows = ssDest.getLastRow();
  
  for (r = 2; r <= rows; r++) {
       
    var option = ssDest.getRange(r, 13).getValue()
    var exists = ssDest.getRange(r, 14).getValue()
    
    if (option == "YES" && exists == "#N/A") {
 
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    d= [year, month, day].join('-');

      
      var mapping =     {
        "sid": ssDest.getRange(r, 7).getValue(),
        "traitType":"SEGMENT",
        //"url":"http://adobe.com",
        "startDate": d,
        "traitAlias":ssDest.getRange(r, 10).getValue()
      }
      
      //make API request
      var options = {'method':'post',
                     'payload': JSON.stringify(mapping),
                     'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                     'muteHttpExceptions':true } 
      var response = UrlFetchApp.fetch("https://api.demdex.com/v1/destinations/"+ssDest.getRange(r, 12).getValue()+"/mappings/",
                                       options) 
                                       
                                       
     ssDest.getRange(r, 16).setValue(response); 
    }
    
       }
    
  


}

function AAMcreateDestination() {
  
  
  
}
