      //delete traits in folder
  function deleteFolderTraits(subFolder) {
     
    //get traits in folder 
     var options = {'method':'get',
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':false } 
    var traits = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/v1/traits/?folderId="+subFolder.folderId,
                                 options)  )
     Logger.log(traits)
    for (var t = 0; t < traits.length; t++) {
         Logger.log(t)
         Logger.log(traits[t])
      var options = {'method':'delete',
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits/"+traits[t].sid,
                                 options) 
    
    Logger.log(response)
         
         }
    
    //delete folder once traits are cleared
     var options = {'method':'delete',
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits/"+subFolder.folderId,
                                 options)  
       Logger.log(response)
       }

function resetAAM() {
  
   var scriptProperties  = PropertiesService.getScriptProperties();
  traitLevel = getConfig();  

  //check for existing authorisation
  accessToken = scriptProperties.getProperty('AAMaccessToken');
  var refreshToken = scriptProperties.getProperty('AAMrefreshToken');

  if (accessToken == undefined ) { 
    accessToken = getAAMtoken();
  }
  
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
    var dataSourceId =  ssInput.getRange(3, 2).getValue() ;
    var parentFolderId =  ssInput.getRange(1, 5).getValue() ; 
  var ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Campaigns & Traits");
  
 
  
  //get Parent Folder subfolders Lvl1
   var options = {'method':'get',
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':false } 
    var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits/"+parentFolderId+"?includeSubFolders=true",
                                 options)  )

      var subFolders = response.subFolders;

  for (var f =0; f < subFolders.length; f++) {
       
    //get subfolders Lvl2
    var options = {'method':'get',
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':false } 
    var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits/"+subFolders[f].folderId+"?includeSubFolders=true",
                                 options)  )

      var subFolders2 = response.subFolders;
    
    for (var f2=0; f2 < subFolders2.length; f2++) {
         deleteFolderTraits(subFolders2[f2])
         }
  
     //delete subfolders lvl1
         deleteFolderTraits(subFolders[f])
         
    
  }
  
 
  
  }
  