function searchTrait(traitIntegrationCode) {

    var options = {'method':'get',
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/traits/ic:" + traitIntegrationCode,
                                 options)  
    
    var responseCode = response.getResponseCode();
    return responseCode;

}


//function to make Trait Creation API request and write results to sheet
function createTrait(traitDefinition, accessToken, rM) {
      
      var options = {'method':'post',
                     'payload': JSON.stringify(traitDefinition),
                     'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                     'muteHttpExceptions':true } 
      response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                       options) 
      
      ssOutputProd.getRange(rM, 9).setValue( traitDefinition.name );
      ssOutputProd.getRange(rM, 10).setValue( traitDefinition.traitRule );
      ssOutputProd.getRange(rM, 11).setValue( traitDefinition.integrationCode );
      ssOutputProd.getRange(rM, 12).setValue( traitDefinition.description );
      ssOutputProd.getRange(rM, 13).setValue( traitDefinition.comments );
      ssOutputProd.getRange(rM, 14).setValue( JSON.parse(response).sid );  
      ssOutputProd.getRange(rM, 15).setValue(response.getResponseCode())
      ssOutputProd.getRange(rM, 16).setValue(response.getContentText())

      //make function return rM (the row results are written on) so it can be incremented for each of the 3 traits created for every product
      return rM;
      
      }

function AAMproductTraits() {
  
  var scriptProperties  = PropertiesService.getScriptProperties();

  //get AAM API authorisation token
  accessToken = getAAMtoken();
 
  ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  
  var ttl =  "720" //ssInput.getRange(2, 9).getValue() ;
  
  ssOutputProd = SpreadsheetApp.getActive().getSheetByName("OUTPUT - products");
  var rows = ssOutputProd.getLastRow();
  rM = 3;
  

    for (var r=3; r<=rows; r++) {
      
      //read product name
      var reportSuite = ssOutputProd.getRange(r, 1).getValue();
      var productName = ssOutputProd.getRange(r, 3).getValue();
      var productCategory = ssOutputProd.getRange(r, 2).getValue();
      var productSKU = ssOutputProd.getRange(r, 4).getValue();
      var dataSourceId = ssOutputProd.getRange(r, 6).getValue() ;
      var folderId = ssOutputProd.getRange(r, 7).getValue() ;
      var folderName = ssOutputProd.getRange(r, 8).getValue();
      
     
      //define trait details depending on whether operating on Product or Category level
      if (productName == "") {
        // when operating on Category level the Product Name and SKU columns will be empty, therefore define trait details for the Category

              //if product name from AA is "not set" or "unavailable" skip
              if ( productCategory.match("unavailable") !== null || productCategory.match("not set") !== null ) {continue} ;
       traitName = folderName + "_Onsite_Visit_ProductCategory_"+ productCategory;
       traitDescription = "Product views of products in category '" +productCategory+ '"';
       traitIntegrationCode = productCategory+"_"+reportSuite;
       traitComments = "Trait automatically created by GSheet API.";
       traitRule = 'c_evar28 == "' + productCategory + '" AND c_prop21 contains "' + reportSuite +'"' ;

        
      } else {
      
              //if product name from AA is "not set" or "unavailable" skip
              if ( productName.match("unavailable") !== null || productName.match("not set") !== null ) {continue} ;
       traitName = folderName + "_Onsite_Visit_Product_"+ productName;
      //var traitName = folderName + "_Onsite_Visit_Product_Category"+ categoryName;
       traitDescription = "Product views of '"+ productName + "' of category '" +productCategory+ "' and SKU "+productSKU;
       traitIntegrationCode = productName+"_"+reportSuite;
       traitComments = "Trait automatically created by GSheet API.";
       traitRule = 'c_evar29 == "' + productName + '" AND c_prop21 contains "' + reportSuite +'"' ;
      }
      
      //split traitName in components to reconstruct for Intent and Basket traits regardless of product/category level
      var split = traitName.split("_");
      
      //definition for Product/Category view action
      var traitDefinitionView = {
        "name": traitName,
        "description": traitDescription,
        "integrationCode": traitIntegrationCode,
        "comments": traitComments,
        "traitType": "RULE_BASED_TRAIT",
        "status": "ACTIVE",
        "dataSourceId": dataSourceId,
        "folderId": folderId,
        "traitRule": traitRule,
        "categoryId": 0,
        "ttl": ttl.toString()
        // ,"type": 1,
        //"pid": 0,
        //"crUID": 0,
        //"upUID": 0,
        //"createTime": 0,
        //"updateTime": 0,
        //"algoModelId": 0,
        //"thresholdValue": 0,
        //"thresholdType": "ACCURACY"
      }
      
      //definition for Product/Category ecommence Intent To Buy action
      var traitDefinitionIntent = {
        "name": split[0]+"_"+split[1]+"_Onsite_Click_"+"Buy now_"+split[4]+"_"+split[5],
        "description": "Purchase intent for product(s) defined as e38 ='Click to purchase/Buy it Now' OR e56 ='Buy In Store'",
        "integrationCode": traitIntegrationCode+"BuyIntent",
        "comments": traitComments,
        "traitType": "RULE_BASED_TRAIT",
        "status": "ACTIVE",
        "dataSourceId": dataSourceId,
        "folderId": folderId,
        "traitRule": traitRule +  ' AND (c_e38 > 0 OR c_e56 > 0)',
        "categoryId": 0,
        "ttl": ttl.toString()
      }

      //definition for Product/Category ecommence Add To Basket action
      var traitDefinitionBasket = {
        "name": split[0]+"_"+split[1]+"_Onsite_Click_"+"Add To Basket_"+split[4]+"_"+split[5],
        "description": "Add To Basket action for the product(s) based on AA's standard scAdd event",
        "integrationCode": traitIntegrationCode+"BasketAdd",
        "comments": traitComments,
        "traitType": "RULE_BASED_TRAIT",
        "status": "ACTIVE",
        "dataSourceId": dataSourceId,
        "folderId": folderId,
        "traitRule": traitRule +  ' AND c_scAdd > 0',
        "categoryId": 0,
        "ttl": ttl.toString()
      }      
      Logger.log(traitIntegrationCode + " " + rM)
      //check whether Trait for Product/Category View already exists by Integration Code, skip creation if so and move on to the next row of the loop
      var IClookupResponse = searchTrait(traitIntegrationCode);
      
      if (IClookupResponse == 200) {
        ssOutputProd.getRange(r, 15).setValue( "---" )
        ssOutputProd.getRange(r, 16).setValue( "Trait already exists, skipping" )
        continue;
      } 
      
      //create Product/Category View action trait      
      rM = createTrait(traitDefinitionView, accessToken, rM) +1;
      //create Product/Category ecommerce Intent To Buy action trait
      rM = createTrait(traitDefinitionIntent, accessToken, rM) +1;
      //create Product/Category Add To Basket action trait
      rM = createTrait(traitDefinitionBasket, accessToken, rM) +1;
      
            
      
    }
    

}

