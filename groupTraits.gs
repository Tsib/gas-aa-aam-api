function createGroupTraits() {

var scriptProperties  = PropertiesService.getScriptProperties();
traitLevel = getConfig();  
  
  //get a fresh token
  accessToken = getAAMtoken();
  var ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Campaigns & Traits");
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  var ttl =  ssInput.getRange(2, 9).getValue() ;
  var rows = ssOutput.getLastRow();
  var groupNames = ssOutput.getRange(2, 37, rows, 1).getValues()
  var groups = {};
  var folders = {};
  g = 0;
  
  //define all trait group names as keys in the "group" object to later populate with AdSet IDs
  while (groupNames[g][0] !== "") {
  
  folders[groupNames[g][0]] = []
  groups[groupNames[g][0]] = [];
  g++
  
  }
  
  var groupName = "";
  //determine if creating Placement or Creative groups
  var IDlocation = 0;
  (traitLevel !== "Creative") ? IDlocation = 4 : IDlocation = 6;
  
  //loop through placement rows to create list of IDs for group rule
  for (var r = 3; r <= rows; r++) {
    
    if (ssOutput.getRange(r, 1).getValue() !== "") {
    groupName = ssOutput.getRange(r, 1).getValue();
      groups[groupName].push( ssOutput.getRange(r, IDlocation).getValue() );
      folders[groupName].push( ssOutput.getRange(r, 14).getValue()  ) ;
    }
       }
  
  //loop through list of groups and create Trait definitions

  for (td = 0; td < g; td++) {
       
    //define trait details
    var dataSourceId =  ssInput.getRange(3, 2).getValue() ;
    var traitName = groupNames[td][0];
    var traitDescription = "";
    var traitIntegrationCode = traitName;
    var traitComments = "Trait automatically created by GSheet API. ";
    var AdSetIDs = groups[traitName].join('" OR adID == "')   
    AdSetIDs+= '"'
    var traitRule = 'adID == "' + AdSetIDs ;
    
    var folder =  folders[traitName][0];
    Logger.log(td + " " + folder)
    
    var traitDefinition = {
      "name": traitName,
      "description": traitDescription,
      "integrationCode": traitIntegrationCode,
      "comments": traitComments,
      "traitType": "RULE_BASED_TRAIT",
      "status": "ACTIVE",
      "dataSourceId": dataSourceId,
      "folderId": folder,
      "traitRule": traitRule,
      "categoryId": 0,
      "ttl": ttl.toString()
      // ,"type": 1,
      //"pid": 0,
      //"crUID": 0,
      //"upUID": 0,
      //"createTime": 0,
      //"updateTime": 0,
      //"algoModelId": 0,
      //"thresholdValue": 0,
      //"thresholdType": "ACCURACY"
    }
    
       //make API request
    var options = {'method':'post',
                   'payload': JSON.stringify(traitDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                   options) 
    Logger.log(response)
   //r is the Output row the results will be written on, starting from the top
    var r = 3 + td;
  //check response code in case of token expiration and try once more with new token
  if (response.getResponseCode() == 401) {
    accessToken = getAAMtoken();
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                     options) 
    ssOutput.getRange(r, 21).setValue(response.getResponseCode())
    if (response.getResponseCode() == 200) {
     // ssOutput.getRange(r, 14).setValue( campaignFolder.folderId );
      ssOutput.getRange(r, 15).setValue( traitName );
      ssOutput.getRange(r, 16).setValue( traitRule );
      ssOutput.getRange(r, 17).setValue( traitIntegrationCode );
      ssOutput.getRange(r, 18).setValue( traitDescription );
      ssOutput.getRange(r, 19).setValue( traitComments );
      ssOutput.getRange(r, 21).setValue(response.getResponseCode())
      ssOutput.getRange(r, 22).setValue(response.getContentText())
    } 
    
  } else if (response.getResponseCode() == 200 || response.getResponseCode() == 201) {
     // ssOutput.getRange(r, 14).setValue( campaignFolder.folderId );
      ssOutput.getRange(r, 15).setValue( traitName );
      ssOutput.getRange(r, 16).setValue( traitRule );
      ssOutput.getRange(r, 17).setValue( traitIntegrationCode );
      ssOutput.getRange(r, 18).setValue( traitDescription );
      ssOutput.getRange(r, 19).setValue( traitComments );
      ssOutput.getRange(r, 21).setValue(response.getResponseCode())
      ssOutput.getRange(r, 22).setValue(response.getContentText())
  }
       
       }
  
}