function getConfig() {
  
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  var traitLevel = ssInput.getRange(2, 2).getValue();
  
  return traitLevel;

  
}
